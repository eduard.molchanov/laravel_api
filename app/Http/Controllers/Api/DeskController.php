<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeskStoreRequest;
use App\Http\Resources\DeskResource;
use App\Models\Desk;

use Illuminate\Http\Response;
use Illuminate\Http\Request;


class DeskController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
//        return Desk::all();
        return DeskResource::collection(Desk::all());
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(DeskStoreRequest $request)
    {
        $created_desk = Desk::create($request->validated());
        return new DeskResource($created_desk);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
//        return Desk::find($id);
        return new DeskResource(Desk::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(DeskStoreRequest $request, Desk $desk)
    {
        $desk->update($request->validated());
        return $desk;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Desk $desk)
    {
       $desk->delete();
       return response(null, Response::HTTP_NO_CONTENT);
    }
}
